import React from 'react';

export default (props) => {
    return (
        <button
            className="btn"
            onClick={ props.handler }>
            { props.txt }
        </button>
    );
};
