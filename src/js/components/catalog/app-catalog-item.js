import React from 'react';
import AppActions from '../../actions/app-actions';
import CartButton from '../cart/app-cart-button';

export default (props) => {
    return (
        <div className="col-md-3">
            <h4>{props.item.title}</h4>
            <p>{props.item.summary}</p>
            <p>{props.item.cost}SEK.
            <span className="text-success">
                &nbsp;{props.item.qty && `(${props.item.qty} in cart)`}
            </span>

            </p>
            <CartButton
                handler = {
                    AppActions.addItem.bind(null, props.item)
                }
                txt="Add to cart"
            />
        </div>
    );
};
