import { dispatch, register } from '../dispatchers/app-dispatcher';
import AppActions from '../actions/app-actions';
import { EventEmitter } from 'events';
import AppConstants from '../constants/app-constants';

const CHANGE_EVENT = 'change';
let catalog = [];
let cartItems = [];

for(let i = 0; i< 9; i++){
    catalog.push({
        'id':`Widget${i}`,
        'title':`Widget #${i}`,
        'summary':'A great widget.',
        'cost':`${i}`
    });
}

const cartTotal = (qty = 0, total = 0) => {

    cartItems.forEach(item => {
        qty += item.qty;
        total += item.qty * item.cost;
    });
    return { qty, total };
};

const addItem = ( item ) => {
    const cartItem = findCartItems(item);
    if(!cartItem){
        cartItems.push(Object.assign({ qty:1 }, item));
    } else {
        increaseItem(cartItem);
    }
};

const increaseItem = ( item ) => item.qty++;
const decreaseItem = ( item ) => {
    item.qty--;
    if(item.qty === 0){
        removeItem(item);
    }
};

const removeItem = ( item ) => {
    cartItems.splice(cartItems.findIndex(i => {
        return i === item;
    }),1);
};

const findCartItems = ( item ) => {
    return cartItems.find(cartItem => cartItem.id === item.id);
};

const AppStore = Object.assign(EventEmitter.prototype, {
    emitChange(){
        this.emit( CHANGE_EVENT );
    },
    addChangeListener(callback){
        this.on(CHANGE_EVENT, callback);
    },
    removeChangeListener(callback){
        this.removeListener(CHANGE_EVENT, callback);
    },
    getCart(){
        return cartItems;
    },
    getCatalog(){
        return catalog.map(item=>{
            return Object.assign({}, item, cartItems.find(cItem => cItem.id === item.id));
        });
    },
    getCartTotals(){
        return cartTotal();
    },
    dispatcherIndex:register(function(action){
        switch(action.actionType) {
            case AppConstants.ADD_ITEM :
                addItem(action.item);
            break;
            case AppConstants.REMOVE_ITEM :
            console.log(action.item);
                removeItem(action.item);
            break;
            case AppConstants.INCREASE_ITEM :
                increaseItem(action.item);
            break;
            case AppConstants.DECREASE_ITEM :
                decreaseItem(action.item);
            break;
        }
        AppStore.emitChange();
    })
});

export default AppStore;
