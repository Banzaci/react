module.exports = {
    entry: './src/js/main.js',
    output: {
        path: './dist',
        filename: 'bundle.js',
        publicPath:'/'
    },
    devServer:{
        inline:true,
        contentBase:'./dist'
    },
    devtool: 'inline-source-map',
    module: {
        loaders: [{
            test: /.jsx?$/,
            loader: 'babel',
            exclude: /node_modules/,
            query: {
                presets: ['es2015', 'react']
            }
        }]
    }
};
